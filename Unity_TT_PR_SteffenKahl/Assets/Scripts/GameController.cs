﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TTPP
{
    /// <summary>
    /// Holds Information for the whole Game and Controls general behaviour
    /// </summary>
    public class GameController : MonoBehaviour
    {
        //SETUP THE MANAGER
        // Make GameManager globally accessible
        public static GameController Instance { get; set; }

        void Awake()
        {
            DontDestroyOnLoad(transform.gameObject); //Make sure that the Manager is present in the whole game
            Instance = this;
        }

        //SAVE DATA FOR THE WHOLE GAME
        //Private Variables
        [SerializeField] private int difficulty = 0;
        [SerializeField] private string playerName = "Username";
        [SerializeField] private float effectsVolume = 1.0f;
        [SerializeField] private float musicVolume = 1.0f;

        //Properties
        /// <summary>
        /// The difficulty set by the User
        /// </summary>
        public int Difficulty
        {
            get { return difficulty; }
            set { difficulty = value; }
        }

        /// <summary>
        /// The name of the user, playing the Game
        /// </summary>
        public string PlayerName
        {
            get { return playerName; }
            set { playerName = value; }
        }

        /// <summary>
        /// The Volume Setting for Effects
        /// </summary>
        public float EffectsVolume
        {
            get { return effectsVolume; }
            set { effectsVolume = value; }
        }

        /// <summary>
        /// The Volume Settings for Music
        /// </summary>
        public float MusicVolume
        {
            get { return musicVolume; }
            set { musicVolume = value; }
        }
    }
}