﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TTPP
{
    /// <summary>
    /// Defines basic Capabilities of Blocks
    /// </summary>
    public class Blocks : MonoBehaviour
    {
        [SerializeField]private String blockName = "undefined";

        private void Start()
        {
            blockName = getBlockName();
        }

        public Component getBlockBehaviour()
        {
            string blockType = this.gameObject.tag;
            Component blockBehaviour = null;

            switch (blockType)
            {
                case "Block_Log":
                {
                    blockBehaviour = this.gameObject.GetComponent<Log_Behaviour>();
                    break;
                }
                case "Block_Stone":
                {
                    blockBehaviour = this.gameObject.GetComponent<Stone_Behaviour>();
                    break;
                }
                case "Block_Grass":
                {
                    blockBehaviour = this.gameObject.GetComponent<Grass_Behaviour>();
                    break;
                }
            }

            return blockBehaviour;
        }

        public bool onRightClick()
        {
            string blockType = this.gameObject.tag;
            Component blockBehaviour = null;

            switch (blockType)
            {
                case "Block_Log":
                {
                    this.gameObject.GetComponent<Log_Behaviour>().onRightClick();
                    break;
                }
                case "Block_Stone":
                {
                    this.gameObject.GetComponent<Stone_Behaviour>().onRightClick();
                    break;
                }
                case "Block_Grass":
                {
                    this.gameObject.GetComponent<Grass_Behaviour>().onRightClick();
                    break;
                }
                default:
                    return false;
            }

            return true;
        }
        
        public bool onLeftClick()
        {
            string blockType = this.gameObject.tag;
            Component blockBehaviour = null;

            switch (blockType)
            {
                case "Block_Log":
                {
                    return this.gameObject.GetComponent<Log_Behaviour>().onLeftClick();
                }
                case "Block_Stone":
                {
                    return this.gameObject.GetComponent<Stone_Behaviour>().onLeftClick();
                }
                case "Block_Grass":
                {
                    return this.gameObject.GetComponent<Grass_Behaviour>().onLeftClick();
                }
                default:
                    return false;
            }

            return true;
        }

        public string getBlockName()
        {
            string blockType = this.gameObject.tag;
            string blockName = "undefined";

            switch (blockType)
            {
                case "Block_Log":
                {
                    blockName = this.gameObject.GetComponent<Log_Behaviour>().getName();
                    break;
                }
                case "Block_Stone":
                {
                    blockName = this.gameObject.GetComponent<Stone_Behaviour>().getName();
                    break;
                }
                case "Block_Grass":
                {
                    blockName = this.gameObject.GetComponent<Grass_Behaviour>().getName();
                    break;
                }
            }

            return blockName;
        }
    }

}