﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TTPP
{
    /// <summary>
    /// Controls the Playerobject
    /// </summary>
    public class PlayerController : MonoBehaviour
    {
        CharacterController characterController;

        public float speed = 6.0f;
        public float jumpSpeed = 8.0f;
        public float gravity = 20.0f;

        float horizontalSpeed = 7.0f;
        float verticalSpeed = 7.0f;

        private Vector3 moveDirection = Vector3.zero;

        void Start()
        {
            characterController = GetComponent<CharacterController>();
        }

        void Update()
        {
            //Rotates Player on "X" Axis Acording to Mouse Input
            float h = horizontalSpeed * Input.GetAxis("Mouse X");
            transform.Rotate(0, h, 0);

            //Rotates Player on "Y" Axis Acording to Mouse Input
            float v = verticalSpeed * Input.GetAxis("Mouse Y");
            Camera.main.transform.Rotate(-v, 0, 0);

            if (characterController.isGrounded)
            {
                // We are grounded, so recalculate
                // move direction directly from axes

                moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
                moveDirection *= speed;

                if (Input.GetButton("Jump"))
                {
                    moveDirection.y = jumpSpeed;
                }

            }

            // Apply gravity. Gravity is multiplied by deltaTime twice (once here, and once below
            // when the moveDirection is multiplied by deltaTime). This is because gravity should be applied
            // as an acceleration (ms^-2)
            moveDirection.y -= gravity * Time.deltaTime;

            Vector3 move = new Vector3(0, 0, Input.GetAxisRaw("Vertical") * Time.deltaTime);
            move = this.transform.TransformDirection(move);

            // Move the controller
            characterController.Move(moveDirection * Time.deltaTime);
        }
    }
}