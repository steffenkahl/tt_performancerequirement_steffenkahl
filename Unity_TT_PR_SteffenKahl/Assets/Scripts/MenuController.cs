﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace TTPP
{
    /// <summary>
    /// Controls everything the Mainmenu does
    /// </summary>
    public class MenuController : MonoBehaviour
    {
        public void onGameStart()
        {
            SceneManager.LoadScene("Level_1", LoadSceneMode.Single);
        }

        public void onGameEnd()
        {
            Debug.Log("GAME QUITS...");
            Application.Quit();
        }

        public void onPlayerNameChange()
        {
            TMP_InputField input = FindObjectOfType<TMP_InputField>();
            GameController controller = GameController.Instance;
            controller.PlayerName = input.text;
        }
    }
}
