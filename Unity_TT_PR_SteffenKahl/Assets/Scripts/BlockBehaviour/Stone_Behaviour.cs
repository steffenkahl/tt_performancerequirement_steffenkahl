﻿using System;
using System.Collections;
using System.Collections.Generic;
using TTPP;
using UnityEngine;

namespace TTPP
{

    /// <summary>
    /// Defines the Behaviour of Blocks of the Type "Stone"
    /// </summary>
    public class Stone_Behaviour : Blocks
    {
        private GameController gameController = null;

        private void Start()
        {
            gameController = GameController.Instance;
        }

        public bool onRightClick()
        {
            Boolean successful = true;
            if (gameController.Difficulty >= 2)
            {
                Destroy(this);
            }
            return successful;
        }
        
        public bool onLeftClick()
        {
            Boolean successful = true;
            if (gameController.Difficulty >= 2)
            {
                Destroy(this);
            }
            return successful;
        }

        public string getName()
        {
            return "Stone";
        }
    }
}