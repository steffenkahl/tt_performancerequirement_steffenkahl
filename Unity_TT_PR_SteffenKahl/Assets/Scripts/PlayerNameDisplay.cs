﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using TMPro;
using TTPP;
using UnityEngine;

namespace TTPP
{
    /// <summary>
    /// Changes the displayed Name above a Player
    /// </summary>
    public class PlayerNameDisplay : MonoBehaviour
    {
        private TMP_Text text = null;

        private void Start()
        {
            text = this.GetComponent<TMP_Text>();
            text.text = GameController.Instance.PlayerName;
        }
    }
}